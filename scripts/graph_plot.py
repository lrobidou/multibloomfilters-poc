import os
import matplotlib.pyplot as plt
import cstes_5voisins
import numpy as np

les_x = cstes_5voisins.get_n_divided_by_m()
les_x = les_x[:10]
to_plot = (
    cstes_5voisins.filter_FN
    / (cstes_5voisins.filter_TP + cstes_5voisins.filter_FN)
    * 100
)
plt.plot(
    les_x,
    to_plot[:10],
    "b--",
    label="filter: False Negative Rate",
)

to_plot = (
    cstes_5voisins.multifilters_FN
    / (cstes_5voisins.multifilters_TP + cstes_5voisins.multifilters_FN)
    * 100
)
plt.plot(
    les_x,
    to_plot[:10],
    "b",
    label="multilfter: False Negative Rate",
)

to_plot = (
    cstes_5voisins.filter_FP
    / (cstes_5voisins.filter_FP + cstes_5voisins.filter_TN)
    * 100
)
plt.plot(
    les_x,
    to_plot[:10],
    "g--",
    label="filter: False Positive Rate",
)


to_plot = (
    cstes_5voisins.multifilters_FP
    / (cstes_5voisins.multifilters_FP + cstes_5voisins.multifilters_TN)
    * 100
)
plt.plot(
    les_x,
    to_plot[:10],
    "g",
    label="multiflters: False Positive Rate",
)
# plt.plot(
#     cstes_1voisin.epsilons[:15],
#     cstes_1voisin.one_filter_recalls[:15],
#     "g--",
#     label="Bloom filter: recall",
# )

# plt.xlabel("epsilon (in %)")
# plt.ylabel("%")
# plt.legend(loc="best")
# plt.show()

# plt.plot(
#     cstes_1voisin.epsilons[:15],
#     cstes_1voisin.multi_filters_filesizes[:15],
#     "b",
#     label="multiFilters: size",
# )
# plt.plot(
#     cstes_1voisin.epsilons[:15],
#     cstes_1voisin.one_filter_filesizes[:15],
#     "g--",
#     label="Bloom filter: size",
# )
# plt.xlabel("epsilon (in %)")


plt.xlabel("n / m")
plt.ylabel("%")
plt.legend(loc="best")
plt.show()
