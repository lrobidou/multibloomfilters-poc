import numpy as np
import os

# multifilters_data
multifilters_TP_FP_FN_TN = [
    (87469, 250, 3629, 4766076),
    (87498, 383, 3600, 4765938),
    (87515, 602, 3583, 4765719),
    (87588, 1199, 3510, 4765122),
    (87713, 2780, 3385, 4763543),
    (87634, 5123, 3464, 4761198),
    (87699, 9731, 3399, 4756593),
    (87759, 18622, 3339, 4747713),
    (87908, 42858, 3190, 4723570),
    (87968, 84822, 3130, 4681892),
    (91098, 4766321, 0, 0),
    (88130, 146737, 2968, 4620895),
    (88227, 214414, 2871, 4554233),
    (88304, 301631, 2794, 4468937),
    (88584, 514967, 2514, 4262852),
    (88750, 753807, 2348, 4036009),
    (88876, 900753, 2222, 3897975),
    (89249, 1408919, 1849, 3429555),
    (91098, 4766321, 0, 0),
    (91098, 4766321, 0, 0),
    (91098, 4766321, 0, 0),
    (91098, 4766321, 0, 0),
    (91098, 4766321, 0, 0),
    (90426, 3774394, 672, 1443014),
    (90627, 4282419, 471, 1047885),
    (90778, 4588171, 320, 818531),
    (90822, 4865198, 276, 608923),
]
multifilters_epsilons = range(4, 58, 2)


# filter_data
filter_TP_FP_FN_TN = [
    (87773, 245, 3325, 4766076),  # 4
    (87818, 359, 3280, 4765962),  # 6
    (87832, 506, 3266, 4765815),  # 8
    (87869, 649, 3229, 4765672),
    (87913, 922, 3185, 4765399),
    (87924, 1112, 3174, 4765209),
    (88024, 1464, 3074, 4764857),
    (88100, 2224, 2998, 4764097),
    (88169, 3383, 2929, 4762938),
    (88124, 5031, 2974, 4761290),
    (88265, 8703, 2833, 4757618),
    (88235, 8894, 2863, 4757427),
    (88322, 13245, 2776, 4753076),
    (88399, 18242, 2699, 4748079),
    (88490, 26927, 2608, 4739394),
    (88466, 38288, 2632, 4728033),
    (88630, 55951, 2468, 4710370),
    (88627, 78083, 2471, 4688238),
    (88862, 108799, 2236, 4657522),
    (88916, 150267, 2182, 4616054),
    (89125, 205820, 1973, 4560501),
    (89151, 277140, 1947, 4489181),
    (89287, 366679, 1811, 4399642),
    (89096, 264239, 2002, 4502082),
    (89335, 326758, 1763, 4439563),
    (89348, 403332, 1750, 4362989),
    (89416, 486416, 1682, 4279905),
]

filter_epsilons = range(4, 58, 2)


# separation in multiple array
multifilters_TP, multifilters_FP, multifilters_FN, multifilters_TN = list(
    zip(*multifilters_TP_FP_FN_TN)
)
multifilters_TP = np.array(multifilters_TP)
multifilters_FP = np.array(multifilters_FP)
multifilters_FN = np.array(multifilters_FN)
multifilters_TN = np.array(multifilters_TN)


# separation in multiple array
filter_TP, filter_FP, filter_FN, filter_TN = list(zip(*filter_TP_FP_FN_TN))
filter_TP = np.array(filter_TP)
filter_FP = np.array(filter_FP)
filter_FN = np.array(filter_FN)
filter_TN = np.array(filter_TN)

nb_elem = 5442650


def get_n_divided_by_m():
    result = []
    diff = []  # différence de taille entre les multifilters et filters
    for espilon in range(4, 58, 2):
        filename = "multifilters" + str(espilon / 100)
        m = os.stat(filename).st_size * 8
        filename = "filter" + str(espilon / 100)
        m2 = os.stat(filename).st_size * 8
        diff.append((m2 / m) * 100)
        result.append(nb_elem / m)
    return result