import matplotlib.pyplot as plt
from scipy.special import comb
import numpy as np
from matplotlib.widgets import Slider
from functools import partial


class LoiBinomiale:
    def __init__(self, n, p):
        self.n = n
        self.p = p

    def __eq__(self, k):
        return comb(self.n, k) * self.p ** k * (1 - self.p) ** (self.n - k)

    def __lt__(self, k):
        proba = 0
        for i in range(k):
            proba += self == i
        return proba

    def __le__(self, k):
        return self < k + (self == k)

    def __gt__(self, k):
        return 1 - ((self < k) + (self == k))

    def __ge__(self, k):
        return 1 - (self < k)


def P_FPreq(loi_binomiale, theta, rencontres):
    rencontres_manquantes = int(theta * loi_binomiale.n) - rencontres
    if rencontres_manquantes <= 0:
        return 1
    return loi_binomiale >= rencontres_manquantes


def update(l, theta):
    l.set_ydata([P_FPreq(loi_binomiale, theta, x) for x in les_x])
    fig.canvas.draw_idle()


if __name__ == "__main__":
    n, epsilon = 100, 0.05
    loi_binomiale = LoiBinomiale(n, epsilon)
    les_x = list(range(n))
    les_y = [P_FPreq(loi_binomiale, 0.5, x) for x in les_x]

    fig, ax = plt.subplots()
    plt.subplots_adjust(left=0.25, bottom=0.25)
    (l,) = plt.plot(les_x, les_y)

    plt.xlim((0, 100))
    plt.ylim((0, 1))
    plt.xlabel("proportion of kmer actually in the gene")
    plt.ylabel("P(positif)")

    axamp = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor="lightgoldenrodyellow")
    samp = Slider(axamp, "Theta", 0, 1, valinit=0)
    samp.on_changed(partial(update, l))

    plt.show()
