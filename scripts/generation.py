from random import Random, randint
from math import inf
from numpy.random import randn
from dataclasses import dataclass


# cstes
K = 32
SEED = randint(0, 100000000)


def generate_k_mers(k=K, n=20000, seed=SEED, skip=0):
    if n == 0:
        return
    myrandom = Random()
    myrandom.seed(seed + skip)  # TODO
    s = ""
    for i in range(k):
        s += myrandom.choice("ACTG")
        myrandom.seed(seed + skip + i + 1)
    yield skip, s
    for i in range(1, n):
        s = s[1::] + myrandom.choice("ACTG")
        myrandom.seed(seed + skip + k + i)
        yield skip + i, s


# @dataclass
# class Kmer:
#     pos: str
#     value: float
#     aboundance: int
