import cProfile
import time

from generation import FileToKmers, FilesToFilters
from plot import plot
from MultiFilters import MultiFilters, Filter
from Evaluation import Evaluation, EvaluationOneFilter
from StrikeLengths import StrikeLengths, StrikeLengthsOneFilter

# TODO renommer counter en filter ?
# TODO use log instead of poor prints
# TODO time functions

FILENAMES_AND_ABOUNDANCES = [
    ("data/ecoli3.txt", 17),
    ("data/ecoli2.txt", 13),
    ("data/Listeria phage.txt", 1),
    ("data/Penicillium chrysogenum.txt", 5),
    # ("data/Salmonella enterica.txt", 16),
]

QUERY = "data/Salmonella enterica.txt"


def get_exact_graph(exact_filters, all_kmer_query):
    exact_graph = []
    for aboundance in exact_filters:
        exact_filter = exact_filters[aboundance]
        for x, kmer in enumerate(all_kmer_query):
            if kmer in exact_filter:
                exact_graph.append((x, aboundance))
    return exact_graph


from functools import singledispatch


@singledispatch
def get_stats(
    kind_of_filter, all_kmer_query, exact_filters, min_neighbours, evaluation
):
    raise NotImplementedError("not implemented")


@get_stats.register(MultiFilters)
def _(multifilters, all_kmer_query, exact_filters, min_neighbours, evaluation):
    strike_lengths = StrikeLengths(all_kmer_query, multifilters)
    strike_lengths = strike_lengths.get_longest_strikes()
    proba_filtered_in, _ = strike_lengths.split(min_neighbours)
    # multifilters.save("multifilters" + str(multifilters.epsilon))
    return evaluation.run(proba_filtered_in)


@get_stats.register(Filter)
def _(one_filter, all_kmer_query, exact_filters, min_neighbours, evaluation):
    strike_lengths = StrikeLengthsOneFilter(all_kmer_query, one_filter)
    proba_filtered_in, _ = strike_lengths.split(min_neighbours)
    # one_filter.save("filter" + str(one_filter.epsilon))
    return evaluation.run(proba_filtered_in)


def main():
    # start = time.time()

    exact_filters = FilesToFilters(FILENAMES_AND_ABOUNDANCES).create_exact_filters()
    all_kmer_query = list(FileToKmers(QUERY))

    exact_graph = get_exact_graph(exact_filters, QUERY)
    evaluation = Evaluation(all_kmer_query, exact_graph)

    min_neighbours = 5
    filter_results = []
    multifilters_results = []
    for epsilon in range(4, 6, 2):
        epsilon = epsilon / 100

        # one filter
        one_filter = Filter(exact_filters, epsilon)
        filter_results.append(
            get_stats(
                one_filter, all_kmer_query, exact_filters, min_neighbours, evaluation
            )
        )

        # multifilter
        multifilters = MultiFilters(exact_filters, epsilon)
        multifilters_results.append(
            get_stats(
                multifilters, all_kmer_query, exact_filters, min_neighbours, evaluation
            )
        )

        print(filter_results)
        print(multifilters_results)

        # plot(
        #     exact=exact_graph,
        #     strike_lengths=strike_lengths,
        #     min_strike_length=min_neighbours,  # TODO rename en min_neighbours partout
        # )


if __name__ == "__main__":
    # cProfile.run("main()")
    main()
