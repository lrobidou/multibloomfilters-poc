class StrikeLengths:
    def __init__(self, all_kmers_possible, probabilistic_counters):
        self.strike_lengths = {}

        memoized = {}
        for aboundance_class in probabilistic_counters:
            memoized[aboundance_class] = 0

        for i in range(len(all_kmers_possible)):
            for aboundance_class in probabilistic_counters:
                if memoized[aboundance_class]:
                    memoized[aboundance_class] -= 1
                    continue
                bloom_filter = probabilistic_counters[aboundance_class]
                nb_neigbhour = _get_nb_neigbhours_right(
                    all_kmers_possible, i, bloom_filter
                )
                memoized[aboundance_class] = nb_neigbhour
                for j in range(nb_neigbhour):
                    self.add(i + j, aboundance_class, nb_neigbhour)

    def add(self, x, y, strike_length):
        try:
            same_legnth_points = self.strike_lengths[strike_length]
        except KeyError:
            same_legnth_points = {}
            self.strike_lengths[strike_length] = same_legnth_points

        try:
            same_legnth_points[x].append(y)
        except KeyError:
            same_legnth_points[x] = [y]

    def split(self, value=5):
        proba_filtered_in = []
        proba_filtered_out = []
        for aboundance_class in self.strike_lengths:
            coord_dict = self.strike_lengths[aboundance_class]
            if aboundance_class <= value:
                for x in coord_dict:
                    for y in coord_dict[x]:
                        proba_filtered_out.append((x, y))
            else:
                for x in coord_dict:
                    for y in coord_dict[x]:
                        proba_filtered_in.append((x, y))
        return proba_filtered_in, proba_filtered_out

    def get_longest_strikes(self):
        longest_strikes_only = {}
        pos_encountered = set()
        strike_lengths = list(self.strike_lengths.keys())
        strike_lengths.sort(reverse=True)
        for strike_length in strike_lengths:
            same_legnth_points = self.strike_lengths[strike_length]
            for x in same_legnth_points:
                if not x in pos_encountered:
                    pos_encountered.add(x)
                    if not strike_length in longest_strikes_only:
                        longest_strikes_only[strike_length] = {}
                    longest_strikes_only[strike_length][x] = same_legnth_points[x]

        return _InternalStrikeLengthsNoInit(longest_strikes_only)


class StrikeLengthsOneFilter:
    def __init__(self, all_kmers_possible, probabilistic_filter):
        self.strike_lengths = {}
        memoized = 0
        for i in range(len(all_kmers_possible)):
            if memoized:
                memoized -= 1
                continue
            bloom_filter = probabilistic_filter.probabilistic_filter
            nb_neigbhour = _get_nb_neigbhours_right(all_kmers_possible, i, bloom_filter)
            memoized = nb_neigbhour
            for j in range(nb_neigbhour):
                self.add(i + j, nb_neigbhour)

    def add(self, x, strike_length):
        try:
            same_legnth_points = self.strike_lengths[strike_length]
        except KeyError:
            same_legnth_points = {}
            self.strike_lengths[strike_length] = same_legnth_points

        try:
            same_legnth_points[x].append(1)
        except KeyError:
            same_legnth_points[x] = [1]

    def split(self, value=5):
        proba_filtered_in = []
        proba_filtered_out = []
        for aboundance_class in self.strike_lengths:
            coord_dict = self.strike_lengths[aboundance_class]
            if aboundance_class <= value:
                for x in coord_dict:
                    for y in coord_dict[x]:
                        proba_filtered_out.append((x, y))
            else:
                for x in coord_dict:
                    for y in coord_dict[x]:
                        proba_filtered_in.append((x, y))
        return proba_filtered_in, proba_filtered_out


def _get_nb_neigbhours_right(all_kmers_possible, index, probabilistic_counter):
    nb_neigbhour = 0
    while index < len(all_kmers_possible):
        if all_kmers_possible[index] in probabilistic_counter:
            nb_neigbhour += 1
        else:
            break
        index += 1
    return nb_neigbhour


class _InternalStrikeLengthsNoInit(StrikeLengths):
    """
    Same as StrikeLengths, but take directly strike_lengths as a parameter,
    since if we can provide it directly, there is no need to recompute it.
    """

    def __init__(self, strike_lengths):
        self.strike_lengths = strike_lengths
