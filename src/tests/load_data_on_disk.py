# TODO remove this file, as its sole purpose is to test the bloom filters written on disk to check everything's alright

import pickle

with open("probabilistic_filter", "rb") as fichier:
    probabilistic_filter = pickle.load(fichier)

with open("probabilistic_counters", "rb") as fichier:
    probabilistic_counters = pickle.load(fichier)


print("nombre d'elements dans probabilistic_filter :", len(probabilistic_filter))

somme = 0
for aboundance in probabilistic_counters:
    somme += len(probabilistic_counters[aboundance])
print("nombre d'elements dans probabilistic_counters:", somme)
