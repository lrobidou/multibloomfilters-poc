from StrikeLengths import StrikeLengths


def test_split():
    # k-mers of size 1
    all_k_mers = ["a", "b", "c", "d", "e", "f", "g", "h"]

    # x: all k-mers, sorted by their order of appearance in the gene
    # y: abundance of k-mers
    # (for example, the kmers of index 5, "f", has been seen 1 and 2 times)
    # Note: the example is impossible in real data
    # (but is still possible in our data with a non zero FP)

    # 3 |               x
    # 2 |         x x x
    # 1 | x x x x   x
    # 0 |----------------
    #     0 1 2 3 4 5 6 7
    #     a b c d e f g h

    abundance_as_a_set = {1: {"a", "b", "c", "d", "f"}, 2: {"e", "f", "g"}, 3: {"h"}}
    strike_lengths = StrikeLengths(all_k_mers, abundance_as_a_set)
    accepted, rejected = strike_lengths.split(2)

    assert accepted == [(0, 1), (1, 1), (2, 1), (3, 1), (4, 2), (5, 2), (6, 2)]
    assert rejected == [(5, 1), (7, 3)]


def test_get_longest_strikes():
    all_k_mers = ["a", "b", "c", "d", "e", "f", "g", "h"]
    abundance_as_a_set = {1: {"a", "b", "c", "d", "f"}, 2: {"e", "f", "g"}, 3: {"h"}}
    strike_lengths = StrikeLengths(all_k_mers, abundance_as_a_set)
    longest_strike_lengths = strike_lengths.get_longest_strikes()
    accepted, rejected = longest_strike_lengths.split(0)
    assert accepted == [(0, 1), (1, 1), (2, 1), (3, 1), (4, 2), (5, 2), (6, 2), (7, 3)]
    assert rejected == []