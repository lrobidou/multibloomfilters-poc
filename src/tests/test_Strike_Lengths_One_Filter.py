from StrikeLengths import StrikeLengthsOneFilter


def test_split():
    class FakeFilter:
        def __init__(self, x):
            self.x = x
            self.probabilistic_filter = x

        def __contains__(self, y):
            return y in self.x

    # k-mers of size 1
    all_k_mers = ["a", "b", "c", "d", "e", "f", "g", "h"]

    fake_filter = FakeFilter(all_k_mers[0:2] + all_k_mers[5:])
    strike_lengths = StrikeLengthsOneFilter(all_k_mers, fake_filter)
    accepted, rejected = strike_lengths.split(2)

    assert accepted == [(5, 1), (6, 1), (7, 1)]
    assert rejected == [(0, 1), (1, 1)]