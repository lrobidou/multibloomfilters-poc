# from generation import Gene
# from ProbabilisticCounters import ProbabilisticCounters
# from Evaluation import Evaluation, EvaluationConfig
# from StrikeLengths import StrikeLengths


# def test_evaluation():
#     nb_read = 100000
#     nb_base = 10000  # longueur gene
#     k = 32

#     min_strike_length = 3

#     read_param = {"avg_length": 100, "standard_deviation": 20, "k": k}
#     gene = Gene(seed=0, nb_base=nb_base, nb_read=nb_read, **read_param)
#     actual_aboundances, aboundance_as_a_set = gene.create_exact_counters()

#     probabilistic_counters = ProbabilisticCounters(aboundance_as_a_set)

#     evaluation_config = EvaluationConfig()
#     evaluation = Evaluation(actual_aboundances, aboundance_as_a_set)
#     genestr = str(gene)
#     all_kmers_possible = [
#         genestr[i : i + k] for i in range(len(genestr) - k + 1)
#     ]  # TODO remove duplicata
#     strike_lengths = StrikeLengths(all_kmers_possible, probabilistic_counters)
#     proba_filtered_in, proba_filtered_out = strike_lengths.split(min_strike_length)
#     pre, rec = evaluation.run(evaluation_config, proba_filtered_in)
#     print(pre, rec)