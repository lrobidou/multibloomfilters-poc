# import main
# from generation import Gene
# import random as rd
# import cProfile
# from pybloom_live import BloomFilter
# import matplotlib.pyplot as plt


# def test_aboundance():
#     nb_read = 100
#     nb_base = 10000  # longueur gene

#     read_param = {"avg_length": 100, "standard_deviation": 20, "k": 32}

#     gene_seed = 0

#     gene = Gene(gene_seed, nb_base=nb_base, nb_read=nb_read, **read_param)
#     _, aboundance_reversed, aboundance = main.fill_dict(
#         gene, nb_base, nb_read, read_param
#     )
#     for key in aboundance:
#         for kmer in aboundance[key]:
#             assert key == aboundance_reversed[kmer]
#     for kmer in aboundance_reversed:
#         aboundance_reversed[kmer]


# # def test_get_nb_neighbour_one_direction():
# #     x = [0, 0, 1, 1, 1, 1, 1, 2, 3, 4, 4, 5, 6]
# #     y = [0, 1, 0, 1, 2, 3, 4, 2, 2, 2, 3, 3, 1]
# #     assert main.get_nb_neighbour_one_direction(x, y, 1, False) == 1
# #     assert main.get_nb_neighbour_one_direction(x, y, 1, True) == 0
# #     assert main.get_nb_neighbour_one_direction(x, y, 3, False) == 1
# #     assert main.get_nb_neighbour_one_direction(x, y, 3, True) == 0


# if __name__ == "__main__":
#     test_aboundance()