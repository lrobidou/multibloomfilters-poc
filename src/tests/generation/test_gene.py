from random import randint
import pytest

from generation import Gene


def test_Gene():
    read_param = {"avg_length": 10, "standard_deviation": 3, "k": 32}
    gene = Gene(
        randint(0, 100000000),
        100,
        10,
        **read_param,
    )
    complete_gene = str(gene)
    for read in gene:
        for kmer in read:
            assert kmer[0] == complete_gene.index(kmer[1])


def test_Gene_runtime_error():
    read_param = {"avg_length": 10, "standard_deviation": 3, "k": 101}
    with pytest.raises(RuntimeError):
        _ = Gene(
            randint(0, 100000000),
            100,
            10,
            **read_param,
        )


def test_len():
    gene_length = 100
    read_param = {"avg_length": 10, "standard_deviation": 3, "k": 32}
    gene = Gene(
        randint(0, 100000000),
        gene_length,
        10,
        **read_param,
    )
    assert len(gene) == gene_length


class FakeReadGenerator:
    def __init__(self, list_of_fake_read, nb_base):
        self.list_of_fake_read = list_of_fake_read
        self.k = len(self.list_of_fake_read[0][0])
        self.nb_base = nb_base

    def __iter__(self):
        yield from self.list_of_fake_read

    def __len__(self):
        return self.nb_base


class FakeRead:
    def __init__(self, fake_kmers):
        self.fake_kmers = fake_kmers
        self.position_table = {
            "a": 0,
            "b": 1,
            "c": 2,
            "d": 3,
            "e": 4,
            "f": 5,
            "g": 6,
            "h": 7,
        }

    def __iter__(self):
        for kmer in self.fake_kmers:
            yield self.position_table[kmer], kmer

    def __len__(self):
        return len(self.fake_kmers)

    def __getitem__(self, i):
        return self.fake_kmers[i]


def test_create_exact_counters():
    fake_reads = [
        FakeRead(["a", "b", "c", "d"]),
        FakeRead(["b", "d"]),
        FakeRead(["a", "g", "c"]),
        FakeRead(["f", "b", "c", "e"]),
        FakeRead(["a", "b", "f", "g"]),
        FakeRead(["a", "b", "h"]),
    ]
    fake_read_generator = FakeReadGenerator(fake_reads, 8)

    actual_aboundances, aboundance_as_a_set = Gene.create_exact_counters(
        fake_read_generator
    )

    assert aboundance_as_a_set == {
        1: {"e", "h"},
        2: {"d", "f", "g"},
        3: {"c"},
        4: {"a"},
        5: {"b"},
    }
    assert actual_aboundances == [4, 5, 3, 2, 1, 2, 2, 1]
