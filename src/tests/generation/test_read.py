from random import randint
import pytest

from generation import Gene
from generation.Read import Read


def test_size():
    gene_length = 100
    nb_kmers = 2000
    read_param = {"avg_length": 10, "standard_deviation": 3, "k": 32}
    gene = Gene(
        randint(0, 100000000),
        gene_length,
        10,
        **read_param,
    )
    read = Read(gene, read_param["k"], nb_kmers, 0)
    assert read.size() == nb_kmers


def test_invalid_nb_kmers():
    gene_length = 100
    nb_kmers = -1
    read_param = {"avg_length": 10, "standard_deviation": 3, "k": 32}
    gene = Gene(
        randint(0, 100000000),
        gene_length,
        10,
        **read_param,
    )

    with pytest.raises(RuntimeError):
        _ = Read(gene, read_param["k"], nb_kmers, 0)