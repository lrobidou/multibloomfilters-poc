from collections import Counter

from generation import FileToKmers, FilesToFilters


def test_kmer_generation_gene0():
    expected_reads = [
        "AZERT",
        "ZERTY",
        "ERTYU",
        "RTYUI",
        "TYUIO",
        "YUIOP",
        "UIOPQ",
        "IOPQS",
        "OPQSD",
        "PQSDF",
        "QSDFG",
        "SDFGH",
        "DFGHJ",
        "FGHJK",
        "GHJKL",
        "HJKLM",
        "JKLMW",
        "KLMWX",
    ]
    read_generator = FileToKmers("src/tests/data/gene0.txt", k=5)
    for i, read in enumerate(read_generator):
        assert read == expected_reads[i]
    for i, read in enumerate(read_generator):
        assert read == expected_reads[i]


def test_kmer_generation_gene1():
    expected_reads = [
        "ASDZEGHTD",
        "SDZEGHTDC",
        "DZEGHTDCD",
        "ZEGHTDCDG",
        "EGHTDCDGJ",
        "GHTDCDGJG",
        "HTDCDGJGB",
        "TDCDGJGBF",
        "DCDGJGBFD",
        "CDGJGBFDD",
        "DGJGBFDDX",
        "GJGBFDDXS",
    ]
    read_generator = FileToKmers("src/tests/data/gene1.txt", k=9)
    for i, read in enumerate(read_generator):
        assert read == expected_reads[i]
    for i, read in enumerate(read_generator):
        assert read == expected_reads[i]


def test_create_exact_filters():
    aboundance_gene0 = 1
    aboundance_gene1 = 2
    filenames_and_aboundances = [
        ("src/tests/data/gene0.txt", aboundance_gene0),
        ("src/tests/data/gene1.txt", aboundance_gene1),
    ]
    kmer_gene0 = [
        "AZERTYUIOP",
        "ZERTYUIOPQ",
        "ERTYUIOPQS",
        "RTYUIOPQSD",
        "TYUIOPQSDF",
        "YUIOPQSDFG",
        "UIOPQSDFGH",
        "IOPQSDFGHJ",
        "OPQSDFGHJK",
        "PQSDFGHJKL",
        "QSDFGHJKLM",
        "SDFGHJKLMW",
        "DFGHJKLMWX",
    ]
    kmer_gene1 = [
        "ASDZEGHTDC",
        "SDZEGHTDCD",
        "DZEGHTDCDG",
        "ZEGHTDCDGJ",
        "EGHTDCDGJG",
        "GHTDCDGJGB",
        "HTDCDGJGBF",
        "TDCDGJGBFD",
        "DCDGJGBFDD",
        "CDGJGBFDDX",
        "DGJGBFDDXS",
    ]
    all_kmers = Counter(aboundance_gene0 * kmer_gene0 + aboundance_gene1 * kmer_gene1)
    exact_filters = FilesToFilters(filenames_and_aboundances).create_exact_filters(k=10)
    for kmer in all_kmers:
        aboundance = all_kmers[kmer]
        assert kmer in exact_filters[aboundance]
