from MultiFilters import Filter, MultiFilters


EXACT_COUNTERS = {
    1: {"aaa", "aab", "aac", "aad"},
    2: {"baa", "bab", "bac", "bad"},
    3: {"caa"},
}


def test_MultiFilters():

    multifilters = MultiFilters(EXACT_COUNTERS, epsilon=0.05)
    for aboundance_class in multifilters:
        bloom_filter = multifilters[aboundance_class]
        for kmer in EXACT_COUNTERS[aboundance_class]:
            assert kmer in bloom_filter


def test_Filter():
    one_filter = Filter(EXACT_COUNTERS, epsilon=0.05)
    for aboundance_class in EXACT_COUNTERS:
        for kmer in EXACT_COUNTERS[aboundance_class]:
            assert kmer in one_filter