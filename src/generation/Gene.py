from random import Random, randint
from numpy.random import randn
from generation.Read import Read


class Gene:
    def __init__(self, seed, nb_base=6000, nb_read=100, **kwargs):
        self.seed = seed
        self.nb_base = nb_base
        self.nb_read = nb_read
        self.avg_read_length = kwargs["avg_length"]
        self.read_length_standard_deviation = kwargs["standard_deviation"]
        self.k = kwargs["k"]
        self.coverage = [0 for _ in range(self.nb_base)]
        if self.nb_base - self.k < 0:
            raise RuntimeError(
                "You asked for kmers of size k="
                + str(self.k)
                + " in a gene of size "
                + str(nb_base)
                + ". This is not possible. k should be greater than nb_base."
            )
        self.string = self._tostring()

    def create_exact_counters(self):
        actual_aboundances = [0] * (len(self) - self.k + 1)  # debug

        # let's fill kmer_to_aboundance
        kmer_to_aboundance = {}  # TODO : use counter ?
        for read in self:
            for kmer in read:
                actual_aboundances[kmer[0]] += 1
                if not kmer_to_aboundance.get(kmer[1], None):
                    kmer_to_aboundance[kmer[1]] = 0
                kmer_to_aboundance[kmer[1]] += 1

        # let's reverse kmer_to_aboundance to get aboundance_to_kmer
        aboundance_to_kmer = {}
        for kmer in kmer_to_aboundance:
            k_mer_aboundance = kmer_to_aboundance[kmer]
            try:
                aboundance_to_kmer[k_mer_aboundance].add(kmer)
            except KeyError:
                aboundance_to_kmer[k_mer_aboundance] = {kmer}

        return actual_aboundances, aboundance_to_kmer

    def _tostring(self):
        myrandom = Random()
        myrandom.seed(self.seed)
        return "".join([myrandom.choice("ACTG") for _ in range(self.nb_base)])

    def __str__(self):
        return self.string

    def __len__(self):
        return self.nb_base

    def _get_start_and_end_of_a_read(self, read_length):
        """
        cas nominal :
        on place la position moyenne du read sur le gene (materialisée ici par un x)
        .........................x.......
        on met length/2 elements à droite (des y):
        .......................yyx.......
        on met length/2 elements à gauche (des y):
        .......................yyxzz.....

        cas limites:
        pas assez de place à droite (on en met plus à gauche)
        pas assez de place à gauche (on en mets plus à droite)
        pas assez dans les deux: on renvoit tout le gene (même si c'est un cas normalement impossible)
        """
        # indice du milieu des reads dans le Gene
        avg_index_of_read = randint(0, self.nb_base - self.k + 1)
        start_index = int(avg_index_of_read - read_length / 2)
        overflow = 0

        if start_index < 0:
            overflow = abs(start_index)
            start_index = 0

        end_index = start_index + read_length + overflow
        overflow = 0

        if end_index > self.nb_base - self.k:
            overflow = end_index - (self.nb_base - self.k)
            end_index -= overflow

        start_index -= overflow

        if start_index < 0:
            raise RuntimeError(
                "Impossible d'extraire un read du génome"
                " : la taille du génome est trop petite,"
                " le paramètre k trop grand ou la taille du read trop grande."
            )
        return start_index, end_index

    def __iter__(self):
        for _ in range(self.nb_read):
            length = int(
                max(
                    randn() * self.read_length_standard_deviation
                    + self.avg_read_length,
                    0,
                )
            )

            start_index, end_index = self._get_start_and_end_of_a_read(length)
            nb_kmers = end_index - start_index
            yield Read(self, self.k, nb_kmers, start_index)

    def __getitem__(self, val):
        return self.string[val]