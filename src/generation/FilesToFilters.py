from generation.constantes import K


class CharYielder:
    """
    Iterable that yields character from a file.
    """

    def __init__(self, filename, skipfunction):
        self.filename = filename
        self.skipfunction = skipfunction

    def __iter__(self):
        with open(self.filename, "r") as fichier:
            for ligne in fichier:
                if not self.skipfunction(ligne):
                    for c in ligne.rstrip():
                        yield c


class FileToKmers:
    def __init__(self, filename, limit=0, k=K):
        self.filename = filename
        self.k = k
        self.limit = limit

    def __iter__(self):
        # return True if ligne x should be ignored
        skip = lambda x: (not x) or x.startswith("#") or x.startswith(">")
        char_yielder = CharYielder(self.filename, skip)
        current_read = ""

        i = 0
        # first read
        for c in char_yielder:
            if self.limit and i >= self.limit:  # TODO test ?
                break

            if len(current_read) < self.k:
                current_read += c
                if len(current_read) >= self.k:
                    yield current_read
            else:
                current_read = current_read[1:] + c
                yield current_read

            i += 1


class FilesToFilters:
    def __init__(self, filenames_and_aboundances):
        self.filenames_and_aboundances = filenames_and_aboundances

    def create_exact_filters(self, k=K):
        # let's fill kmer_to_aboundance
        kmer_to_aboundance = {}  # TODO : use counter ?
        for filename, aboundance in self.filenames_and_aboundances:
            for kmer in FileToKmers(filename, k=k):
                if kmer in [
                    "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN",
                    "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",
                ]:  # TODO remove
                    break
                if not kmer in kmer_to_aboundance:
                    kmer_to_aboundance[kmer] = 0
                kmer_to_aboundance[kmer] += aboundance

        # let's reverse kmer_to_aboundance to get aboundance_to_kmer
        aboundance_to_kmer = {}
        for kmer in kmer_to_aboundance:
            k_mer_aboundance = kmer_to_aboundance[kmer]
            if not k_mer_aboundance in aboundance_to_kmer:
                aboundance_to_kmer[k_mer_aboundance] = set()
            aboundance_to_kmer[k_mer_aboundance].add(kmer)

        return aboundance_to_kmer
