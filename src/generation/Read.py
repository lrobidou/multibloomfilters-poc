from generation.constantes import K


class Read:
    """
    Contains nb_kmers kmers of size k, starting at position start_index.
    (and the one at start_index+1, start_index+2...)
    """

    def __init__(self, gene, k=K, nb_kmers=2000, start_index=0):
        self.gene = gene
        self.k = k
        self.nb_kmers = nb_kmers
        self.start_index = start_index
        if self.nb_kmers <= 0:
            raise RuntimeError("A read cannot have less than 0 kmer in it.")

    def __iter__(self):
        for i in range(self.start_index, self.start_index + self.nb_kmers + 1):
            yield i, self.gene[i : i + self.k]

    def size(self):
        return self.nb_kmers