import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
from StrikeLengths import StrikeLengths

# def is_isolated(all_kmers_possible, index, probabilistic_counter):
#     # TODO : pourquoi il arrive de ne pas avoir d'erreur ?
#     # je m'attends a toujours en avoir une (le premier test au moins)
#     try:
#         if all_kmers_possible[index - 1] in probabilistic_counter:
#             return False
#     except Exception as e:
#         print("WARN : ", e)
#     try:
#         if all_kmers_possible[index + 1] in probabilistic_counter:
#             return False
#     except Exception as e:
#         print("WARN : ", e)
#     return True


def plot(exact=None, strike_lengths=None, min_strike_length=5):  # strategie="best"):
    _, ax = plt.subplots()

    lines = []

    proba_filtered_in, proba_filtered_out = strike_lengths.split(min_strike_length)
    if exact:
        (exact_plot,) = ax.plot(
            *zip(*exact),
            "bo",
            markersize=3,
            label="real aboundance curve",
        )
        lines.append(exact_plot)
    # proba_filtered_out += (
    #     proba_filtered_in  # decommenter pour afficher *l'ensemble* des points en rouge
    # )
    (proba_removed_plot,) = ax.plot(
        *zip(*proba_filtered_out),
        "rx",
        markersize=2,
        label="removed probabilistic aboundance points",
    )
    lines.append(proba_removed_plot)
    (proba_plot,) = ax.plot(
        *zip(*proba_filtered_in),
        "cx",
        markersize=2,
        label="probabilistic aboundance points",
    )
    lines.append(proba_plot)

    plt.subplots_adjust(left=0.3)
    plt.xlabel("starting position of the kmer in the queried sequence")
    plt.ylabel("aboundance of the kmer")
    plt.legend(loc="best")

    # lines = [exact_plot, proba_plot, proba_removed_plot]

    # Make checkbuttons for all plotted lines with correct visibility
    rax = plt.axes([0.05, 0.4, 0.21, 0.15])
    labels = [str(line.get_label()) for line in lines]
    visibility = [line.get_visible() for line in lines]
    check = CheckButtons(rax, labels, visibility)

    def change_visibility(label):
        index = labels.index(label)
        lines[index].set_visible(not lines[index].get_visible())
        plt.draw()

    check.on_clicked(change_visibility)

    plt.show()


def show_aboundance(
    gene, scanned, probabilistic_counters, k
):  # TODO rename gene to "request"
    genestr = str(gene)
    all_kmers_possible = [
        genestr[i : i + k] for i in range(len(genestr) - k + 1)
    ]  # TODO remove duplicata
    # from_aboundance_to_list(gene, scanned, probabilistic_counters, k)
    strike_lengths = StrikeLengths(all_kmers_possible, probabilistic_counters)
    # strike_lengths = strike_lengths.get_longest_strikes()

    exact_graph = []
    for x in range(len(scanned)):
        exact_graph.append((x, scanned[x]))
    plot(exact_graph, strike_lengths, 3)
