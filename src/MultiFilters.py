from pybloom_live import BloomFilter
import pickle


class Filter:
    def __init__(self, exact_counters, epsilon=0.05):
        self.espilon = epsilon
        nb_total_elements = sum(
            (len(exact_counters[aboundance]) for aboundance in exact_counters)
        )
        self.probabilistic_filter = BloomFilter(nb_total_elements, epsilon)
        for aboundance in exact_counters:
            exact_counter = exact_counters[aboundance]
            for kmer in exact_counter:
                self.probabilistic_filter.add(kmer)

    def __contains__(self, x):
        return x in self.probabilistic_filter

    def save(self, filename):
        with open(filename, "wb") as fichier:
            pickle.dump(self.probabilistic_filter, fichier)


class MultiFilters:
    def __init__(self, exact_counters, epsilon=0.05):
        self.probabilistic_counters = {}
        self.espilon = epsilon
        for aboundance in exact_counters:
            exact_counter = exact_counters[aboundance]
            self.probabilistic_counters[aboundance] = BloomFilter(
                len(exact_counter), epsilon
            )
            for kmer in exact_counter:
                self.probabilistic_counters[aboundance].add(kmer)

    def __iter__(self):
        yield from self.probabilistic_counters

    def __getitem__(self, x):
        return self.probabilistic_counters.get(x)

    def save(self, filename):
        with open(filename, "wb") as fichier:
            pickle.dump(self.probabilistic_counters, fichier)