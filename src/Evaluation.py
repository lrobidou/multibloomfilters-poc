class EvaluationConfig:
    def __init__(self):
        pass


# src/tests/test_evaluation.py 0.9976118452475721 0.6285485003510883
# src/tests/test_evaluation.py 0.996859296482412 1.0
# 0.9959852256303195 0.9977477477477478


class Evaluation:
    def __init__(self, all_kmer_query, exact_graph):
        self.all_kmer_query = all_kmer_query
        self.exact_graph_x_to_y = {}
        for point in exact_graph:
            x, y = point
            try:
                self.exact_graph_x_to_y[x].append(y)
            except KeyError:
                self.exact_graph_x_to_y[x] = [y]

    def run(self, points):
        """
        Return the precision and recall of a prediction.
        """
        # 10 gene
        # une couverture diff
        # couv à 2, couv à 100
        # metagenome => rapport de presence 1 à 100
        # generer plein de read => chaque po couvere en moyenne 10 fois (en theorie tous indexés)
        # refaire ce que j'ai fait
        # faire en plus : generer des kmer aléatoire

        # TODO: faire une expe avec un frequence plus importnate de kmer
        # TODO evoquer: supprimer des kmers impossible
        TP, FP, FN, TN = 0, 0, 0, 0
        to_test_x_to_y = {}

        for point in points:
            x, y = point
            try:
                to_test_x_to_y[x].append(y)
            except KeyError:
                to_test_x_to_y[x] = [y]

        for x in range(len(self.all_kmer_query)):
            if x in self.exact_graph_x_to_y:
                if x in to_test_x_to_y:
                    TP += 1
                    FP += len(to_test_x_to_y[x]) - 1
                else:
                    FN += 1
            else:
                if x in to_test_x_to_y:
                    FP += len(to_test_x_to_y[x])
                else:
                    TN += 1

        # for true_x, true_y in self.exact_graph:
        #     if not true_x in x_to_y:
        #         FN += 1  # there is something here, but we predict nothing
        #     else:
        #         # there is something and we predict something
        #         # but is it what we thought, or did we made a mistake ?
        #         if true_y in x_to_y[true_x]:
        #             # at least one of our prediction is correct
        #             TP += 1
        #             # everything else (if it exists is FP)
        #             FP += len(x_to_y[true_x]) - 1
        #             if len(x_to_y[x]) - 1 > 0:
        #                 n_fp += 1
        #         else:
        #             # complete mistake
        #             FN += len(x_to_y[x])
        # print("pas indexe = ", pas_indexe)
        # return TP / (TP + FP), TP / (TP + FN)
        print(TP, FP, FN, TN)
        return TP, FP, FN, TN


class EvaluationOneFilter:
    def __init__(self, all_kmer_query, exact_graph):
        self.all_kmer_query = all_kmer_query
        self.exact_graph_x_to_y = {}
        for point in exact_graph:
            x, y = point
            try:
                self.exact_graph_x_to_y[x].append(y)
            except KeyError:
                self.exact_graph_x_to_y[x] = [y]

    def run(self, points):
        """
        Return the precision and recall of a prediction.
        """
        TP, FP, FN, TN = 0, 0, 0, 0

        to_test_x_to_y = {}

        for point in points:
            x, y = point
            try:
                to_test_x_to_y[x].append(y)
            except KeyError:
                to_test_x_to_y[x] = [y]

        for x in range(len(self.all_kmer_query)):
            if x in self.exact_graph_x_to_y:
                if x in to_test_x_to_y:
                    TP += 1
                else:
                    FN += 1
            else:
                if x in to_test_x_to_y:
                    FP += 1
                else:
                    TN += 1
        print(TP, FP, FN, TN)
        return TP, FP, FN, TN
        # return TP / (TP + FP), TP / (TP + FN)